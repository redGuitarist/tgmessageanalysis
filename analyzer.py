#!/usr/bin/python3
import parse

ifile = open("data.txt", "r", encoding="utf8")

countVB = 0
countID = 0

totalVB = 0
totalID = 0

charsVB = 0
charsID = 0

#format of input file
format_string = "out: {} datetime: {}-{}-{} {}:{}:{} text: {}"
msgs = []

for line in ifile.readlines():
    msgs.append(parse.parse(format_string, line))
    
for msg in msgs:
    #print(msg)
    if msg:
        if (msg[0] == 'True'):
            totalVB += 1
            charsVB += len(msg[7])
            if ":3" in msg[7]:
                countVB += 1
        else:
            totalID += 1
            charsID += len(msg[7])
            if ":3" in msg[7]:
                countID += 1

total = totalVB + totalID
count = countVB + countID
chars = charsVB + charsID

print("Total messages: {} (Voffka: {} - {}%, Daeer: {} - {}%)".format(total, totalVB, totalVB/total * 100, totalID, totalID/total * 100))
print("Messages containing {}: {} (Voffka: {} - {}%, Daeer: {} - {}%)".format(":3", count, countVB, countVB/count * 100, countID, countID/count * 100))
print("{}% of messages contain {} (Voffka: {}%, Daeer: {}%)".format(count/total * 100, ":3", countVB/totalVB * 100, countID/totalID * 100))
print("Average length of message: {} (Voffka: {}, Daeer: {})".format(chars/total, charsVB/totalVB, charsID/totalID))
print("% in characters: Voffka - {}%, Daeer - {}%".format(charsVB/chars * 100, charsID/chars * 100))
