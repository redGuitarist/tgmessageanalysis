from telethon import TelegramClient, sync

# read constants from file (api_id, api_hash, phone, username)
with open("config.txt", "r") as cfile:
    for line in cfile.readlines():
        exec(line)

# creating client
client = TelegramClient("session", api_id, api_hash)
client.start()

# signing in
if not client.is_user_authorized():
    client.send_code_request(phone)
    client.sign_in(phone, input('Enter code: '))

# opening file
file = open("data.txt", "wb")

daeer = client.get_entity(username)

messages = client.get_messages(daeer, limit=100)
total = messages.total
print("Total: ", total, "\n")

processed = 0

while processed < total:
    messages = client.get_messages(daeer, limit=100, add_offset=processed)
    processed += 100
    print("Processed: {}".format(processed))
    for msg in messages:
        if hasattr(msg, "fwd_from") and msg.fwd_from is None:
            file.write("out: {} datetime: {} text: {}\n".format(msg.out, msg.date, msg.message).encode("UTF-8"))


#for msg in client.iter_messages(username):
#    if hasattr(msg, "fwd_from") and msg.fwd_from is None:
#            file.write("out: {} datetime: {} text: {}\n".format(msg.out, msg.date, msg.message).encode("UTF-8"))

file.close()
client.disconnect()
